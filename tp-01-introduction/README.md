# Formation ELK

## TP-01: Introduction

### Quizz

<details>
    <summary>1) A quoi sert Elasticsearch ?</summary>
    *Réponse: Stockage, recherche et aggréghation de données sous forme de documents*
</details>

<details>
    <summary>2) A quoi correspond le K de ELK</summary>
    *Réponse: Kibana*
</details>

<details>
    <summary>3) Composer un workflow ELK à l'aide des termes suivants:
- shipper
- base de donnée
- client / utilisateur
- log
- alerting
- enrichissement
- visualisation</summary>
    *Réponse: *
    ```ascii
    log -> shipper -> enrichisement -> base de données -> visualisation -> client / utilisateur
    ```
</details>