# Formation ELK

## TP-02: Elasticsearch

### Quizz

<details>
    <summary>1) Quelle rêgle vous semble évidente pour le nombre de master nodes ?</summary>
    *Réponse: nombre impair > 3 (quorum en cas de split brain)*
</details>


<details>
    <summary>2) Puis je avoir 4 data nodes + 3 master nodes ?</summary>
    *Réponse: Oui la répartition est même assez bonne*
</details>


<details>
    <summary>3) faire un schema avec 1 seul point d'entrée, chaque donnée répliquée 2 fois</summary>
    Réponse: 
    ```ascii
        app -> master -> data 1
                      -> data 2
                      -> data 3
    ```
    ```json
        "settings": {
          "index": {
            "number_of_shards": "5",
            "number_of_replicas": "3"
          }
        }
    ```
</details>


<details>
    <summary>4) Quelle est la resource la plus importante pour ES quand on selectionne un type de serveur phy ou virtuel ?</summary>
    *Réponse: la mémoire physique / RAM*
</details>
