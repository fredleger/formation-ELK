# Formation ELK

## TP-04: Kibana

### Creation de votre stack

*Instructions fournies par le formateur*

#### Pré-requis

* Putty
* Winscp

### Import de données

```shell
curl -H 'Content-Type: application/x-ndjson' --user elastic -XPOST 'http://localhost:9200/gfi-2017/formations/_bulk' --data-binary @formations-2017.json
curl -H 'Content-Type: application/x-ndjson' --user elastic -XPOST 'http://localhost:9200/gfi-2016/formations/_bulk' --data-binary @formations-2016.json
curl -H 'Content-Type: application/x-ndjson' --user elastic -XPOST 'http://localhost:9200/gfi-2015/formations/_bulk' --data-binary @formations-2015.json
```

### Discover

<details>
    <summary>1. Trouver le nombre de fromations sur 3 ans</summary>
    *Réponse: *
    [ici][1]
</details>
<details>
    <summary>2. Trouver le nombre de formation suivies par des personnes se prénomant "Alfred"</summary>
    *Réponse: *
    [ici][2]
</details>
<details>
    <summary>3. Celles suivie par un "Alfred" entre Sept. et Oct. 2016</summary>
    [ici][3]
</details>

### Analyze

<details>
    <summary>1. Répresenter un nuage de tags des 10 prénoms les plus courants ces 2 dernières années glissantes</summary>
    *Réponse: *
    [ici][4]
</details>

<details>
    <summary>2. Representer un histograme en batons du nombre de formation par années (2015 à 2017)</summary>
    *Réponse: *
    [ici][5]
</details>

### Timelion

<details>
    <summary>1. Répresenter une coubre sur 5 ans montrant l'évolution du nombre de formations maximum vs le RNB par habitant en France (NY.GNP.PCAP.CD à la banque mondiale)</summary>
    *Réponse: *
    [ici][6]
</details>

* Ensuite enregistrez cette courbe timelion en tant qu'expression de dashboard

### Graph

<details>
    <summary>1. Essayez de trouver un ou plusieurs biais dans les données des indexs</summary>
    *Réponse: *
    - Il y a beaucoup de Martin en nom de famille
    - Il y a beaucoup de Kevin en prénom
    - Le cours le plus suivi est ELK-01
    - On peut aussi le vérifier via l'api :
    ```
    {
    "query": {
        "query_string": {
            "query": "ELK"
        }
    },
    "controls": {
        "use_significance": true,
        "sample_size": 2000,
        "timeout": 5000
    },
    "connections": {
        "vertices": [
            {
                "field": "formation.keyword",
                "size": 10,
                "min_doc_count": 1
            },
            {
                "field": "lastname.keyword",
                "size": 5,
                "min_doc_count": 1
            },
            {
                "field": "firstname.keyword",
                "size": 10,
                "min_doc_count": 1
            }
        ]
    },
    "vertices": [
        {
            "field": "formation.keyword",
            "size": 10,
            "min_doc_count": 1
        },
        {
            "field": "lastname.keyword",
            "size": 5,
            "min_doc_count": 1
        },
        {
            "field": "firstname.keyword",
            "size": 10,
            "min_doc_count": 1
        }
    ]
}
```
</details>

### Dashboard

1. Construisez un dashboard avec tous les éléments conlléctés aux étapes précedentes
2. Génerer un rapport PDF

### Monitoring

1. Passez en revue les rubriques de cette section
2. Trouver quelle taille occuper l'index gfi-2015 sur le disque

### Management

1. Passez en revue les différentes rubriques de cette section
2. Créer un utilisateur avec accès en lecture seule
3. changer le mot de passe de "elastic"

### Dev Tools

<details>
    <summary>1. Regarder le mappping de l'index gfi-2017</summary>
    *Réponse: *
    ```
    GET gfi-2017/formations/_mapping
    ```
</details>

<details>
    <summary>2. Supprimer tous les indexs</summary>
    *Réponse: *
    ```
    DELETE gfi-*
    ```
</details>



[1]: http://127.0.0.1:5601/app/kibana#/discover?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:'2014-12-31T23:00:00.000Z',mode:absolute,to:'2017-12-31T23:00:00.000Z'))&_a=(columns:!(_source),index:'gfi-*',interval:auto,query:'',sort:!('@timestamp',desc))

[2]: http://127.0.0.1:5601/app/kibana#/discover?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:'2014-12-31T23:00:00.000Z',mode:absolute,to:'2017-12-31T23:00:00.000Z'))&_a=(columns:!(_source),filters:!(('$state':(store:appState),meta:(alias:!n,disabled:!f,index:'gfi-*',key:firstname,negate:!f,value:Alfred),query:(match:(firstname:(query:Alfred,type:phrase))))),index:'gfi-*',interval:auto,query:'',sort:!('@timestamp',desc))

[3]: http://127.0.0.1:5601/app/kibana#/discover?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:'2016-08-13T18:06:25.468Z',mode:absolute,to:'2016-11-17T02:30:17.208Z'))&_a=(columns:!(_source),filters:!(('$$hashKey':'object:8405','$state':(store:appState),meta:(alias:!n,disabled:!f,index:'gfi-*',key:firstname,negate:!f,value:Alfred),query:(match:(firstname:(query:Alfred,type:phrase))))),index:'gfi-*',interval:auto,query:'',sort:!('@timestamp',desc))

[4]: http://127.0.0.1:5601/app/kibana#/visualize/create?type=tagcloud&indexPattern=gfi-*&_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:now-2y,mode:quick,to:now))&_a=(filters:!(),linked:!f,query:(query_string:(analyze_wildcard:!t,query:'*')),uiState:(),vis:(aggs:!((enabled:!t,id:'1',params:(),schema:metric,type:count),(enabled:!t,id:'2',params:(field:firstname.keyword,order:desc,orderBy:'1',size:20),schema:segment,type:terms)),listeners:(),params:(maxFontSize:72,minFontSize:18,orientation:single,scale:linear),title:'New%20Visualization',type:tagcloud))

[5]: http://127.0.0.1:5601/app/kibana#/visualize/create?type=histogram&indexPattern=gfi-*&_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:'2014-12-31T23:00:00.000Z',mode:absolute,to:'2017-12-31T23:00:00.000Z'))&_a=(filters:!(),linked:!f,query:(query_string:(analyze_wildcard:!t,query:'*')),uiState:(),vis:(aggs:!((enabled:!t,id:'1',params:(),schema:metric,type:count),(enabled:!t,id:'2',params:(customInterval:'2h',extended_bounds:(),field:'@timestamp',interval:y,min_doc_count:1),schema:segment,type:date_histogram)),listeners:(),params:(addLegend:!t,addTimeMarker:!f,addTooltip:!t,defaultYExtents:!f,legendPosition:right,mode:stacked,scale:linear,setYExtents:!f,times:!()),title:'New%20Visualization',type:histogram))

[6]: http://127.0.0.1:5601/app/timelion#?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:now-5y,interval:auto,mode:quick,timezone:Europe%2FBerlin,to:now))&_a=(columns:2,interval:auto,rows:2,selected:0,sheet:!('.es(index%3Dgfi-*).max().label(%22Nombre%20de%20formations%22).color(%22orange%22),%20.wbi(%22FR%22,%20%22NY.GNP.PCAP.CD%22).divide(10000)'))
