# Formation ELK

## TP-10: Painless

### TP

<details>
  <summary>1. Mettre en place un système qui permet de tagguer automatiquement un document de type "formations" s'il s'agit d'un formation 'ELK-01'. Utilisez un script painless inline</summary>
  *Réponse: *
  ```json
    POST gfi-2017/formations/1
    {
      "query": {
        "function_tag_elk": {
          "script_tag_elk": {
            "script": {
              "lang": "painless",
              "inline": "if (ctx._source.formation == 'ELK-01') {
                          ctx._source.elk_ready = 'true'
                         }
                         else {
                          ctx._source.elk_ready = false
                         }"
            }
          }
        }
      }
    }
  ```
</details>

<details>
  <summary>2. Même question avec un script sous forme de fichier</summary>
  *Réponse: *
  1. Créer le fichier suivant:
  ```painless
  if (ctx._source.formation == 'ELK-01') {
    ctx._source.elk_ready = 'true'
  }
  else {
    ctx._source.elk_ready = false
  }
  ```
  2. Le transférer dans &lt;elasticsearch home&gt;/config/scripts/is_elk_ready
  3. Envoyer la requête suivante:
```
POST gfi-2017/formations/8
{
  "query": {
    "function_tag_elk": {
      "script_tag_elk": {
        "script": {
          "lang": "painless",
          "file": "is_elk_ready"
        }
      }
    }
  }
}
```
</details>



