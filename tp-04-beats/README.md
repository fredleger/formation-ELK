# Formation ELK

## TP-05: Beats

### Quizz

<details>
    <summary>1) quel(s) outil(s) installer pour monitorer un service apache sous windows ?</summary>
    Réponse: Winlogbeat + apachebeat + filebeat     
</details>


<details>
    <summary>2) construire un workflow qui permet de récupérer les logs d'un service d'enrichir le champ user_location avec ses coordonées GPS approximatifs, et de le diffuser à la fois vers un rapport Kibana et une base redis de métadonnées utilisateur</summary>
    Réponse: 
        <pre>
        logs -> filebeat (geoip) -> logstash -> redis
                                             -> elasticsearch -> kibana
        </pre>
</details>
