# Formation ELK

## TP-08: Alerting

### TP

<details>
  <summary>1. Mettre en place un processus d'alerting permettant d'envoyer par slack le nombre d'Arthur qui ce sont inscrits depuis 10 minutes</summary>
  *Réponse: *
  1. Mettre en place la configuration slack dans le fichier de configuration d'elasticsearch & redémarer le service
    ```
    xpack.notification.slack:
      account:
        monitoring:
          url: https://hooks.slack.com/services/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
    ```
  2. Envoyer le document suivant

```json
PUT _xpack/watcher/watch/elk-2
{
  "trigger": {
    "schedule": {
      "interval": "1m"
    }
  },
  "input": {
    "search": {
      "request": {
        "indices": "gfi-*",
        "types" : [ "formations" ],
        "body": {
          "size": 0,
          "query": {
            "bool": {
              "must": { "match": { "firstname": "arthur" }},
              "filter": { "range": { "@timestamp": { "gte": "now-10m/d" }}}
            }
          }
        }
      }
    }
  },
  "condition": {
    "compare": {
      "ctx.payload.hits.total": {
        "gt": 0
      }
    }
  },
  "actions": {  
    "log" : {  
      "logging" : {
        "text" : "{{ ctx.payload.hits.total }} new Arthur(s) registred - executed at {{ctx.execution_time}}",
        "level": "warn"
      }
    },
    "notify-slack" : {
      "transform" : {},
      "throttle_period" : "5m",
      "slack" : {
        "account": "monitoring",
        "message" : {
          "to" : [ "#elk", "@webofmars" ], 
          "text" : "{{ ctx.payload.hits.total }} new Arthur(s) registred (:laughing:)" 
        }
      }
    }
  }
}
```

</details>
