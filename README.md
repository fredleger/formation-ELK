# Formation ELK GFI

## TP-01: Introduction
## TP-02: Elasticsearch
## TP-03: Logstash
## TP-04: Beats
## TP-06: Kibana
## TP-08: Recherche avancée
## TP-09: Alerting
## TP-10: Painless
## TP-11: Full

## How to use

```
git clone https://gitlab.com/webofmars/formation-ELK.git
```
