# Formation ELK-01

## Module 08: Recherche Avancée

### TP

En utilisant l'interface de Kibana et les données du TP-06 :

<details>
    <summary>1. Trouver une formation suivie par une personne prénomée et/ou nommée *'Olivier'*</summary>
    *Réponse: *
    ```
    'olivier' ou 'firstname:olivier OR lastname:olivier'
    ```
</details>

<details>
    <summary>2. Trouver les utilisateurs dont le prénom commence par 'Oliv'</summary>
    * Réponse: *
    ```
    firstname:(oliv.* or /^oliv$/)
    ```
</details>

<details>
    <summary>3. Trouver les utilisateurs dont le nom de famille est situé entre 'Am' et 'Ax'</summary>
    * Réponse: *
    ```
    lastname:[Am TO Ax]
    ```
</details>

<details>
    <summary>4. Trouver le ou les personnes qui :
    - ont un nom de famille entre Am et Ax
    - ont suivi ou vont suivre une formation en 2017
    - le prénom commence par une lettre aprés 'J'
    - n'ont pas suivi la formation ELK-01
    </summary>
    *Réponse: *
    ```
    lastname:[Am TO Ax] AND @timestamp:>2016-12-31 AND firstname:>k NOT formation:ELK-01
    ```
</details>
