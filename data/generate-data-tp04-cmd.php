<?php

require_once './vendor/fzaninotto/faker/src/autoload.php';

$faker = Faker\Factory::create('fr_FR');

$iter = 300;

for ($i=0; $i < $iter; $i++) {


    $thisFormation["@timestamp"] = date_format($faker
                                                ->dateTimeInInterval('2017-01-01', '+ 365 days'),
        'Y-m-d\TH:i:s\Z');

    $thisFormation["firstname"]  = $faker
                                    ->optional($weight = 0.9, $default = 'Kevin')
                                    ->firstName();

    $thisFormation["lastname"]   = $faker
                                    ->optional($weight = 0.9, $default = 'Martin')
                                    ->lastName();

    $thisFormation["formation"]  = $faker
                                    ->optional($weight = 0.75, $default = 'ELK-01')
                                    ->bothify('???-###');

    print '{ "index": { "_id": '.$i.'}}'.PHP_EOL;
    print json_encode($thisFormation).PHP_EOL;
}
