<?php

require_once './vendor/fzaninotto/faker/src/autoload.php';

$faker = Faker\Factory::create('fr_FR');

$purchases = array();
$iter = 10000;

for ($i=0; $i < $iter; $i++) {

    $thisPurchase["transactionId"]          = $faker->unique()->randomNumber(9);
    $thisPurchase["transactionDateTime"]    = date_format($faker->dateTimeThisMonth(), 'Y-m-d H:i:s');

    $thisPurchase["userId"]                 = $faker
                                                ->optional(0.88, 666)
                                                ->randomNumber(8);

    $thisPurchase["basketSize"]             = $faker
                                                ->optional($weight = 0.6, $default = 3)
                                                ->randomDigitNotNull();

    $thisPurchase["purchaseSize"]           = $faker->numberBetween(1, $thisPurchase["basketSize"]);
    
    $thisPurchase["purchaseAmount"]         = $faker
                                                ->optional($weight = 0.66, $default = 33)
                                                ->randomNumber(4);
    
    $thisPurchase["purchasePlace"]          = array(
            "lat" => $faker
                        ->latitude(42.00, 51.00),
            "lon" => $faker
                        ->longitude(-5.00, 8.00)
                                                    );
    
    array_push($purchases, $thisPurchase);
}

$json = json_encode($purchases);

print $json.PHP_EOL;
