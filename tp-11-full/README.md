# TP04: Kibana

Lançons notre 1ère stack ELK !

*Contexte:* Site e-marketing on a besoin de produire des tableaux de bord sur l'évolution des ventes

## credentials

- elastic/changeme

## run it

```
docker-compose up
```

## Utilisation de l'API

### Ingest API: injecter de la "fausse" donnée

```shell
curl -XPUT -H 'Content-Type: application/x-ndjson' http://localhost:8888/ --data-binary @datasource.json
```

### Mapping API: Récuperer le mapping par default

```
curl --user elastic:changeme -o logstash-mapping.json http://localhost:9200/logstash-*/_mappings
```

### indices API: suppression d'indexs

```
curl --user elastic:changeme -XDELETE 'http://localhost:9200/_template/logstash-*'
curl --user elastic:changeme -XDELETE 'http://localhost:9200/logstash-*'
```

## Kibana: découverte et prise en main de Kibana + X-pack

### discover

- Voir les enregistrements utiliser quelques filtres
[Reponse](http://localhost:5601/goto/7209d8625c9e2ab058f221f6a962c071)

- Trouver le nombre de paniers avec plus de 3 achats cette semaine
[Reponse](http://localhost:5601/goto/82bc9d97b40e65680b3c33761bba19f9)

### vizualize

- Creer un camenbert de l'efficacité de la transformation (BasketSize vs PurchaseSize)
- Suivre l'evolution du nombre de transactions avec le temps
- Suivre l'evolution du nombre de transactions avec le temps en distinguant la taille des paniers
- Evolution du montant des transactions avec le temps en distinguant les plus gros clients
- Creer une table de données permettant de suivre le montant moyen pour les paniers de 0 à 3, 4 à 8, 9 à 12 items

voir réponses lors de l'import plus loin

### Dashboard

- Présenter les graphiques crées précement sous la forme d'un dashboard
- Appliquer un theme dark ^^
- Générer un PDF et le télécharger

Réponses:
- voir import plus bas
- PDF: Dashboard > dashName > Reporting

## Aller plus loin

### mappings

- Modifier le mapping de manière à prendre en compte les coordonées géographiques
- Creer une carte des achats

*Réponses:*

## ajuster le template

```
curl --user elastic -XPUT -H 'Content-Type: application/x-ndjson' 'http://localhost:9200/_template/logstash-*' --data-binary @logs-mapping-geo.json
```

## re-indexer

```shell
curl -XPUT -H 'Content-Type: application/x-ndjson' http://localhost:8888/ --data-binary @datasource.json
```

### importer un dashboards et/ou des visualisations

Management > Saved Objects > Import
Utilisez export.json pour voir un dashboard répondant aux questions ci-dessus

