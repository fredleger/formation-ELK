# Formation ELK

## TP-03: Logstash

### TP

*N.B.* : Ce TP peut-être fait en local ous dans le cloud

<details>
    <summary>1) ecrire un service qui convertie une entrée stdin en sortie stdout json</summary>
    Réponse: voir [q1.conf](q1.conf)
    ```shell
    logstash -f q1.conf
    ```
</details>

<details>
    <summary>2) ecrire un batch qui permet de calculer les rates de posts twitter sur les mots clés gfi/training/elastic/webofmars</summary>
    Réponse: voir [q2.conf](q2.conf)
    ```shell
    logstash -f q2.conf
    ```
</details>

<details>
    <summary>3) ecrire un batch qui permet de compter des entrées non structurées "date - xxx object-name"</summary>
    Réponse: voir [q3.conf](q3.conf)
    ```shell
    logstash -f q3.conf
    ```
</details>